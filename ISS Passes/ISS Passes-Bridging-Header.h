//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

/*
 Using Afnetworking 3.0 pods written in Objective-C language to perform Networking tasks.
 This bridging is used to bridge Objective-C library to Swift.
 */
#import "AFNetworking/AFNetworking.h"
