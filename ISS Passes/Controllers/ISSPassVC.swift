//
//  ViewController.swift
//  ISS Passes
//
//  Created by Muhammad Daniyal on 1/17/18.
//  Copyright © 2018 IBM. All rights reserved.
//

import UIKit
import AFNetworking
import CoreLocation

class ISSPassVC: UIViewController,CLLocationManagerDelegate {
    
    //Outlet for button to give it a corner radius and animate the color.
    @IBOutlet weak var getPassesBtn : UIButton!
    
    //Outlet for UpComingIssPasses button to animate it from center to top
    @IBOutlet weak var upcomingIssPasses : UILabel!
    
    //Outlet for top constraint of UpComingIssPasses button to play with its priority for animation
    @IBOutlet weak var upcomingIssPassesTopConstraint : NSLayoutConstraint!
    
    //Outlet for center constraint of UpComingIssPasses button to play with its priority for animation
    @IBOutlet weak var upcomingIssPassesCenterConstraint : NSLayoutConstraint!
    
    //Outlet for tableView
    @IBOutlet weak var tableView : UITableView!
    
    //Outlet for slider label
    @IBOutlet weak var slider : UISlider!
    
    //Outlet for latitude label
    @IBOutlet weak var latitude : UILabel!
    
    //Outlet for longitude label
    @IBOutlet weak var longitude : UILabel!
    
    //Outlet for altitude label
    @IBOutlet weak var altitude : UILabel!
    
    //Outlet for noOfPass label
    @IBOutlet weak var noOfPass : UILabel!
    
    //Outlet for activity Indicator used for animating while request call
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    
    //Outlet for tableView bottom border
    @IBOutlet weak var tableViewBottomBorder : UIView!
    

    //Array of IssPassModel to hold data to display after request call
    var issPasses = [IssPassModel]()

    // Used to start getting the users location
    let locationManager = CLLocationManager()
    
    //Initializing a bool variable to true for one time animation of UpcomingIssPasses heading
    var upcomingIssPassesIsCenter = true

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Initializing the noOfPass default value to be 50
        noOfPass.text = "50"
        
        //Setting up the button color to be blue initially
        self.getPassesBtn.backgroundColor = UIColor.init(red: 49/255, green: 164/255, blue: 1, alpha: 1.0)
        
        //Converting a square button to circle
        getPassesBtn.layer.cornerRadius = getPassesBtn.frame.height/2
        
        //Setting up datasource and delegate of the tableView to be self.
        tableView.dataSource = self
        tableView.delegate = self
        
        //Hiding tableView initially.
        self.tableView.isHidden = true
        
        //Hiding the bottom border of tableview initially.
        self.tableViewBottomBorder.isHidden = true

        //Hiding the activity Indicator initially.
        self.activityIndicator.isHidden = true
        
        //Setting up the delegates of CLLocationManager to be self.
        locationManager.delegate = self
        
        // For use when the app is open
        locationManager.requestWhenInUseAuthorization()
        
   
    }
    
    //Action of Get button
    @IBAction func getPasses(_ sender : UIButton){
        
        //Showing the activityIndicator
        activityIndicator.isHidden = false
        
        //Animating it
        activityIndicator.startAnimating()
        
        //Checking if the upcoming iss passes heading is in center than ,
        //This check will be run only one time.
        if(upcomingIssPassesIsCenter == true){

            //Changing the priority of upcomingIssPassesCenterConstraint to low so it may imply to another constraint which is top from supervieqw
            self.upcomingIssPassesCenterConstraint.priority = .defaultLow
            
            
            //Animating the upcoming iss passes heading from center to top
            UIView.animate(withDuration: 2.0, animations: {
                
                self.upcomingIssPassesTopConstraint.priority = .defaultHigh
                
                self.view.layoutIfNeeded()
                
            }, completion: { (true) in
                
                UIView.animate(withDuration: 1.0, animations: {
                    
                    //Showing the table view and it's border
                    self.upcomingIssPassesIsCenter = false
                    
                    self.tableView.isHidden = false
                    
                    self.tableViewBottomBorder.isHidden = false
                })
                
                
                
            })
            
            
            
            
            
            
        }
        //Animating the button color from blue to orange and vice versa
        if(self.getPassesBtn.backgroundColor == UIColor.init(red: 49/255, green: 164/255, blue: 1, alpha: 1.0))
        {
        UIView.animate(withDuration: 0.5) {
            self.getPassesBtn.backgroundColor = UIColor.orange
        }
        }
        else{
            UIView.animate(withDuration: 0.5) {
                self.getPassesBtn.backgroundColor = UIColor.init(red: 49/255, green: 164/255, blue: 1, alpha: 1.0)
            }
        }

        //Filling up  the params with the result of getLocation()
        var params = getLocation()
        
        //Setting up the labels
        latitude.text = "\(params[0])"
        longitude.text = "\(params[1])"
        altitude.text = "\(params[2])"
        
        //This callback function returns an array filled with ISSPassModel which further helps in populating the tableView
        IssPassService.sharedInstance.getPasses(lat: params[0], long: params[1], alt: params[2], n: Int(noOfPass.text!)!, success: { (responseArray) in
            
            
            self.issPasses = responseArray
            
            //Reloading the tableView with the issPasses array
            self.tableView.reloadData()
            
            //Hiding the activity indicator and also stopping it to animate
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            
        }) { (dataTask, error) in
            
            //Showing an alert with the error message if the api is failed to call
            
            let alertController = UIAlertController(title: "Sorry!", message: error.localizedDescription, preferredStyle: .alert)
            
            let actionBtn = UIAlertAction(title: "OK", style: .default, handler: nil)
          
            alertController.addAction(actionBtn)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
   
    
    // If we have been deined access give the user the option to change it
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }

    // Show the popup to the user if we have been deined access
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Background Location Access Disabled",
                                                message: "In order to deliver pizza we need your location",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    //This method brings back the current location of the device
    func getLocation() -> [Double] {
        
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorized){
            
            if let currentLocation = locationManager.location {
//                activityIndicator.startAnimating()
                
                print("Longitude \(currentLocation.coordinate.longitude) \n")
                print("Latitude \(currentLocation.coordinate.latitude)")
                print("Altitude \(currentLocation.altitude.magnitude)")
                
                return [currentLocation.coordinate.latitude, currentLocation.coordinate.longitude, currentLocation.altitude.magnitude]
                

            }
        }
        return []
    }
    
    //Action method for slider
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        var currentValue = Int(sender.value)
        
        noOfPass.text = "\(currentValue)"
    }
    

}

//Datasources and delegates of table view are seperation with extension in order to keep the code clean
extension ISSPassVC : UITableViewDataSource,UITableViewDelegate{
    
    //Returns no. of rows depedent on the size of issPassArray
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return issPasses.count
    }
    
    //Setting up with values of riseTime and duration in the cell.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PassCell", for: indexPath) as! ISSPassTableViewCell
        
        cell.riseTime.text = issPasses[indexPath.row].riseTime
        
        cell.duration.text = "\(issPasses[indexPath.row].duration!) seconds"
        
        return cell
  
    }
    
    //Setting up the height to cell.
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 87
        
    }
    
}

