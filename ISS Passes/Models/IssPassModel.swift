//
//  IssPassModel.swift
//  ISS Passes
//
//  Created by Muhammad Daniyal on 1/17/18.
//  Copyright © 2018 IBM. All rights reserved.
//

import UIKit

class IssPassModel: NSObject {
    
    var duration : Int!
    
    var riseTime : String!
    
     init(dict : [String : Any ]) {
        
     
        if let duration = dict["duration"] as? Int{
            
            self.duration = duration
            
        }
        //Converting time to human readable
        if  let timeResult = (dict["risetime"] as? Double) {
            let date = NSDate(timeIntervalSince1970: timeResult)
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .medium //Set time style
            dateFormatter.dateStyle = .medium //Set date style
           // dateFormatter.timeZone = NSTimeZone() as TimeZone!
            let localDate = dateFormatter.string(from: date as Date)
            self.riseTime = localDate
        }
    }
    
    
    
    

}
