//
//  IssPassService.swift
//  ISS Passes
//
//  Created by Muhammad Daniyal on 1/17/18.
//  Copyright © 2018 IBM. All rights reserved.
//

import UIKit
import AFNetworking

class IssPassService: NSObject {
    
    //Created a singleton object to be used across the application
    static var sharedInstance = IssPassService()
    
    var passes = [IssPassModel]()
    
    //Limiting the creation of object of the singleton class
    private override init() {
        
    }
    
    //This function returns a callback with results from api call
    func getPasses(lat : Double , long : Double, alt : Double , n : Int, success : @escaping (_ passes : [IssPassModel])->Void , failure : @escaping (_ dataTask : URLSessionDataTask , _ error : Error)->Void) {
        
        //Declaring the session manager for http calls
        let manager = AFHTTPSessionManager()
        
        var altValue = 1.0
        
        //Checking if the altitude is not zero. Api fails to bring back the result if the altitude is sent zero.
        if(alt > 0){
            altValue = alt
        }
        
        //Setting up the url with the required and optional params
        var url = "http://api.open-notify.org/iss-pass.json?lat=\(lat)&lon=\(long)&alt=\(altValue)&n=\(n)"
        
        print(url)
        //Http get call to the service to bring back the iss passes
        manager.get(url, parameters: nil, success: { (dataTask, responseData) in
            
            //Parsing the response to dictionary
            if let dict = responseData as? [String : Any]{
                
                //Parsing the data associated to key response into response array. The result of which is an array filled with iss passes
                if let responseArray = dict["response"] as? [[String : Any]]{
                    
                //A great map function to parse the whole array to iss pass model
                  self.passes = responseArray.map({ (data) -> IssPassModel in
                    
                    return IssPassModel(dict: data)
                    })
                
                    //Here the call goes to the function with an array of passes to VC
                    success(self.passes)
                }
                
            }
        }) { (dataTask, error) in
            
            //When api fails to call
            failure(dataTask!,error)
        }
        
    }

}
