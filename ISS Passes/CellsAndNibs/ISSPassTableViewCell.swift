//
//  ISSPassTableViewCell.swift
//  ISS Passes
//
//  Created by Muhammad Daniyal on 1/18/18.
//  Copyright © 2018 IBM. All rights reserved.
//

import UIKit

class ISSPassTableViewCell: UITableViewCell {
    
    @IBOutlet var riseTime : UILabel!
    
    @IBOutlet var duration : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
